show databases;
drop database micasapro;
commit;
create database if not exists micasapro;
use micasapro;
show tables;

Create table user(
	id int not null AUTO_INCREMENT,
	name varchar(255) not null,
    email varchar(100) not null UNIQUE,
	password varchar(100) not null,
	email_verified bit not null default 0,
    provider varchar(255) not null,
	provider_id varchar(255),
	PRIMARY KEY (id),
    UNIQUE INDEX email_unique (email),
	UNIQUE INDEX id_UNIQUE (id)
);

Create table user_profile(
	user_id int not null AUTO_INCREMENT,
	firstname varchar(100) not null,
	lastname varchar(100) not null,
	mobilenumber varchar(15) not null,
	role ENUM('ADMIN','CSR','OWNER', 'TENANT'), -- role precedence
	createtime DATETIME DEFAULT CURRENT_TIMESTAMP,
	updatetime DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	deletetime DATETIME,
	subscription_starttime DATETIME,
	subscription_endtime DATETIME,
	PRIMARY KEY (user_id),
    UNIQUE INDEX userid_unique (user_id),
	CONSTRAINT fk_profile_user FOREIGN KEY (user_id) 
		REFERENCES user(id)  
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

Create table property(
	id int not null AUTO_INCREMENT,
    owner_profile_id int not null,
    street1 varchar(50) not null,
	street2 varchar(50),
	landmark varchar(50),
	city varchar(25) not null,
	state varchar(25) not null,
	country varchar(25) not null,
	zip int(6) not null,
	createtime DATETIME DEFAULT CURRENT_TIMESTAMP,
	updatetime DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	deletetime DATETIME,
	PRIMARY KEY (id),
	CONSTRAINT fk_property_owner_profile FOREIGN KEY (owner_profile_id) 
		REFERENCES user_profile(user_id)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

Create table tenant_property(
	id int not null AUTO_INCREMENT,
    property_id int not null,
    tenant_profile_id int not null,
    PRIMARY KEY (id),
    CONSTRAINT fk_tenant_property_profile_id FOREIGN KEY (tenant_profile_id) 
		REFERENCES user_profile(user_id)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	CONSTRAINT fk_tenant_property_id FOREIGN KEY (property_id) 
		REFERENCES property(id)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

Create table property_details(
	id int not null AUTO_INCREMENT,
	property_id int not null,
	rent_amount int not null,
	availability_date DATETIME,
	rental_startdate DATETIME,
	rental_enddate DATETIME,
	PRIMARY KEY (id),
	CONSTRAINT fk_propertydetails_property FOREIGN KEY (property_id) 
		REFERENCES property(id)  
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

Create table property_details_history(
	id int not null AUTO_INCREMENT,
	property_id int not null,
	rent_amount int not null,
	availability_date DATETIME,
	rental_startdate DATETIME,
	rental_enddate DATETIME,
	PRIMARY KEY (id),
	CONSTRAINT fk_propertydetailshistory_property
	FOREIGN KEY (property_id) 
		REFERENCES property(id)  
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

Create table lease_agrement(
	id int not null AUTO_INCREMENT,
	property_id int not null,
	tenant_id int not null, -- tenant id
	leasedocument BLOB,
	link varchar(300),
	type varchar(30),
	security_deposit float(10,2),
	create_time DATETIME,
	startdate DATETIME,
	notice_period DATETIME,
	notification_flag int default 0,
	enddate DATETIME,
	deletetime DATETIME,
	state ENUM('NEW','ACTIVE', 'RENEWED', 'TERMINATED'),
	PRIMARY KEY (id),
	CONSTRAINT fk_leaseagrement_property
	FOREIGN KEY (property_id) 
		REFERENCES property(id)  
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	CONSTRAINT fk_leaseagrement_user_profile
	FOREIGN KEY (tenant_id) 
		REFERENCES user_profile(user_id)  
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

Create table rental_history(
	id int not null AUTO_INCREMENT,
	property_id int not null,
	tenant_id int not null,
	startdate DATETIME not null,
	enddate DATETIME not null,
	month varchar(5) not null,
	paidtime DATETIME,
	paidamount float(10,2),
	pendingamount float(10,2),
	totalamount float(10,2),
	payment_reference varchar(100),
	payment_type Enum ('Cash', 'PayTm', 'GooglePay', 'BankTransfer', 'Cheque'),	
	PRIMARY KEY (id),
	CONSTRAINT fk_rentalhistory_property
	FOREIGN KEY (property_id) 
		REFERENCES property(id)  
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	CONSTRAINT fk_rentalhistory_user_profile
	FOREIGN KEY (tenant_id) 
		REFERENCES user_profile(user_id)  
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

Create table picture(
	id int not null AUTO_INCREMENT,
	document blob not null,
	type varchar(100),
	PRIMARY KEY (id)
);

Create table maintenance(
	id int not null AUTO_INCREMENT,
	property_id int not null,
	tenant_id int not null,
	requestdate DATETIME DEFAULT CURRENT_TIMESTAMP,
	assigned_to varchar(100),
	category Enum ('Electrical', 'Electronic', 'Other'),	
	description varchar(500) not null,
	state Enum ('New', 'InProgress', 'Closed'),	
	picture_ids varchar(100), -- comma seperated pictures list
	PRIMARY KEY (id),
	CONSTRAINT fk_maintenance_property
	FOREIGN KEY (property_id) 
		REFERENCES property(id)  
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	CONSTRAINT fk_maintenance_user_profile
	FOREIGN KEY (tenant_id) 
		REFERENCES user_profile(user_id)  
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

Create table maintenance_comments(
	id int not null AUTO_INCREMENT,
	maintenance_id int not null,
    comment varchar(100) not null,
	comment_by varchar(100) not null,
	posttime DATETIME DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (id),
	CONSTRAINT fk_maintenancecomments_maintenance
	FOREIGN KEY (maintenance_id) 
		REFERENCES maintenance(id)  
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

Create table inspection_history(
	id int not null AUTO_INCREMENT,
	property_id int not null,
	inspectiondate DATETIME,
	assigned_to varchar(100),
	subject varchar(500) not null,
	state Enum ('New', 'InProgress', 'Closed'),	
	visit varchar(100),
	PRIMARY KEY (id),
	CONSTRAINT fk_inspectionhistory_property
	FOREIGN KEY (property_id) 
		REFERENCES property(id)  
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

Create table inspection_checklist(
	id int not null AUTO_INCREMENT,
	inspectionhistory_id int not null,
	testname varchar(100) not null,
	teststatus varchar(100) not null,
	remark varchar(500) not null,
	picture_ids varchar(100), -- comma seperated pictures list
	PRIMARY KEY (id),
	CONSTRAINT fk_inspectionchecklist_inspectionhistory
	FOREIGN KEY (inspectionhistory_id) 
		REFERENCES inspection_history(id)  
		ON DELETE CASCADE
		ON UPDATE CASCADE
);



